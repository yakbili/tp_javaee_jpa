package org.yasser.exercice1;

import java.util.Date;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManager {
	EntityManagerFactory entityManagerFactory = 
			Persistence.createEntityManagerFactory("jpa-test");

	javax.persistence.EntityManager entityManager = entityManagerFactory.createEntityManager();

	public User createUser (String firstName, String lastName, Date dateOfBirth){
		entityManager.getTransaction().begin();
		User user = new User(firstName, lastName, dateOfBirth);
		entityManager.persist(user);
		entityManager.getTransaction().commit();
		return user;

	}

	public User find (long id){
		entityManager.getTransaction().begin();
		User user = entityManager.find(User.class, id);
		entityManager.getTransaction().commit();
		return user;

	}

	public void delete (long id){
		entityManager.getTransaction().begin();
		User user = find(id);
		entityManager.remove(user);
		entityManager.getTransaction().commit();

	}


}
