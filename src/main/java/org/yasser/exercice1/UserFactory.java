package org.yasser.exercice1;

import java.util.Date;

import javax.persistence.EntityManager;

public class UserFactory {
	private EntityManager entityManager;

	public UserFactory(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}
	

	public User createUser (String firstName, String lastName, Date dateOfBirth){
		entityManager.getTransaction().begin();
		User user = new User(firstName, lastName, dateOfBirth);
		entityManager.persist(user);
		entityManager.getTransaction().commit();
		return user;

	}

	public User find (long id){
		entityManager.getTransaction().begin();
		User user = entityManager.find(User.class, id);
		entityManager.getTransaction().commit();
		return user;

	}

	public void delete (long id){
		entityManager.getTransaction().begin();
		User user = find(id);
		entityManager.remove(user);
		entityManager.getTransaction().commit();

	}



}
