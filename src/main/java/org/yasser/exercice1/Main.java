package org.yasser.exercice1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//*******************Tests CRUD ********
		EntityManagerFactory entityManagerFactory = 
				Persistence.createEntityManagerFactory("jpa-test");

		EntityManager emTest = entityManagerFactory.createEntityManager();
		UserFactory userFact = new UserFactory(emTest);



		Calendar calendar = new GregorianCalendar(1995, 5, 20);
		Date date = calendar.getTime();
		User user1 = userFact.createUser("Yasser", "AKBILI", date);
		System.out.println(user1);


		emTest.getTransaction().begin();
		
		System.out.println("Avant Update : " + user1);
		user1.setLastName("DIOP"); //"U"
		System.out.println("Apr�s Update : " + user1);


		emTest.persist(user1); //"C"

		User userTrouve = userFact.find(5);//"R"
		System.out.println("Retrieve : " + userTrouve);




		userFact.delete(5);//"D"

		emTest.getTransaction().commit();

		//Test question 4 - 5 - 6 ***********************************************************
		org.yasser.exercice1.EntityManager em = new org.yasser.exercice1.EntityManager();
		Calendar calendar2 = new GregorianCalendar(1995, 5, 20);
		Date date2 = calendar2.getTime();
		User user2 = new User("Yasser", "Akbili", date2);
		em.createUser("Yasser", "Akbili", date2);
		System.out.println(em.find(1));


	}

}
